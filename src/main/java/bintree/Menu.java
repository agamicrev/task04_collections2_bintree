package bintree;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {

  private static Scanner input = new Scanner(System.in);
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private BinTree binTree;

  public Menu() {
    binTree = new BinTree();
    menu = new LinkedHashMap<String, String>();
    methodsMenu = new LinkedHashMap<String, Printable>();
    menu.put("1", "1 - Insert element");
    menu.put("2", "2 - Remove element");
    menu.put("3", "3 - Print the tree");
    menu.put("4", "4 - All keys");
    menu.put("5", "5 - All values");
    menu.put("6", "6 - Tree size");
    menu.put("7", "7 - Check element presence");
    menu.put("Q", "Q - Exit");

    methodsMenu.put("1", this::insertElement);
    methodsMenu.put("2", this::removeElement);
    methodsMenu.put("3", this::printTree);
    methodsMenu.put("4", this::printKeys);
    methodsMenu.put("5", this::printValue);
    methodsMenu.put("6", this::printTreeSize);
    methodsMenu.put("7", this::checkPresence);
  }

  void outputMenu() {
    System.out.println("\n**************************************");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  private void insertElement() {
    String value;
    String key;
    System.out.print("\nInput your element: ");
    value = input.nextLine();
    System.out.print("\nInput key of this element: ");
    key = input.nextLine();
    binTree.put(key, value);
  }

  private void removeElement() {
    String key;
    System.out.print("\nInput key of this element: ");
    key = input.nextLine();
    binTree.remove(key);
  }

  private void printTree() {
    System.out.println("Your tree: ");
    binTree.print(binTree.getRoot());
  }

  private void printKeys() {
    System.out.println("Keys of each element:\n" + binTree.keySet());
  }

  private void printValue() {
    System.out.println("Values of each element:\n" + binTree.values());
  }

  private void printTreeSize() {
    System.out.println("Tree size: " + binTree.size());
  }

  private void checkPresence() {
    String key;
    System.out.print("\nInput your element:");
    key = input.nextLine();
    if (binTree.containsValue(key) == true) {
      System.out.println("\nThe element is in tree");
    } else {
      System.out.println("\nThe element isn`t in tree");
    }
  }


  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("\nPlease, make your choice: ");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
