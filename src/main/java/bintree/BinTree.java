package bintree;

import java.util.*;

public class BinTree<K extends Comparable, V> implements Map<K, V> {

  boolean containsValue = false;
  private int size = 0;
  private Node<K, V> root;

  public int size() {
    return size;
  }

  public boolean isEmpty() {
    if (size == 0) {
      return true;
    } else {
      return false;
    }
  }

  public boolean containsKey(Object key) {
    Node<K, V> currentNode = root;
    while (currentNode.getKey().compareTo(key) != 0) {
      if (currentNode.compareTo(key) > 0) {
        currentNode = currentNode.left;
      } else {
        currentNode = currentNode.right;
      }
      if (currentNode == null) {
        return false;
      }
    }
    return true;
  }

  public boolean containsValue(Object value) {
    containsValue = false;
    recInOrder(root, value);
    return containsValue;
  }

  Node<K, V> recInOrder(Node<K, V> currentNode, Object value) {
    if (currentNode.getValue().equals(value)) {
      this.containsValue = true;
    } else {

      if (currentNode.left == null || currentNode.right == null) {
        return null;
      }
      recInOrder(currentNode.left, value);
      recInOrder(currentNode.right, value);

      return null;
    }
    return null;
  }

  public V get(Object key) {
    Node<K, V> currentNode = root;
    while (currentNode.getKey().compareTo(key) != 0) {
      if (currentNode.compareTo(key) > 0) {
        currentNode = currentNode.left;
      } else {
        currentNode = currentNode.right;
      }
      if (currentNode == null) {
        return null;
      }
    }
    return currentNode.getValue();
  }

  public V put(K key, V value) {
    if (size == 0) {
      root = new Node<K, V>(key, value);
    } else {
      Node<K, V> currentNode = root;
      Node<K, V> correctBranch;
      correctBranch = checkNode(currentNode, null, key, value);
      while (correctBranch != null) {
        currentNode = correctBranch;
        correctBranch = checkNode(currentNode, correctBranch, key, value);
      }
      if (currentNode.getKey().equals(key)) {
        currentNode.setValue(value);
      } else {
        if (currentNode.compareTo(key) > 0) {
          currentNode.left = new Node<K, V>(key, value);
        } else {
          currentNode.right = new Node<K, V>(key, value);
        }
      }
    }
    size++;
    return null;
  }

  private Node<K, V> checkNode(Node<K, V> currentNode, Node<K, V> correctBranch, K key, V value) {
    if (currentNode.getKey().equals(key)) {
      correctBranch = null;
      currentNode.setValue(value);
      size--;
    } else {
      if (currentNode.compareTo(key) > 0) {
        correctBranch = currentNode.left;
      } else {
        correctBranch = currentNode.right;
      }
    }
    return correctBranch;
  }

  public V remove(Object key) {
    Node<K, V> currentNode = root;
    Node<K, V> parentNode = null;
    V saveValue;
    if (currentNode.getKey().compareTo(key) == 0) {
      saveValue = root.value;
      removeForRoot();
      size--;
      return saveValue;
    }
    while (currentNode.getKey().compareTo(key) != 0) {
      parentNode = currentNode;
      if (currentNode.compareTo(key) > 0) {
        currentNode = currentNode.left;
      } else {
        currentNode = currentNode.right;
      }
      if (currentNode == null) {
        return null;
      }
    }
    saveValue = root.value;
    if (currentNode.left == null && currentNode.right == null) {
      removeNodeWithoutBranch(parentNode, currentNode);
    } else {
      if (currentNode.left != null && currentNode.right == null) {
        removeNodeWithLeftBranch(parentNode, currentNode);
        size--;
        return saveValue;
      }
      if (currentNode.right != null && currentNode.left == null) {
        removeNodeWithRightBranch(parentNode, currentNode);
      } else {
        removeNodeWithTwoBranches(parentNode, currentNode);
      }
    }
    size--;
    return saveValue;
  }

  void removeForRoot() {
    Node<K, V> currentNode = root;
    if (root.left == null && root.right == null) {
      root = null;
    } else {
      if (root.left != null && root.right == null) {
        root = root.left;
      }
      if (currentNode.right != null && currentNode.left == null) {
        root = root.right;
      } else {
        removeNodeWithTwoBranchesForRoot();
      }
    }
  }


  private void removeNodeWithTwoBranchesForRoot() {
    Node<K, V> copyNode = root.right;
    while (copyNode.left != null) {
      copyNode = copyNode.left;
    }
    remove(copyNode.key);
    copyNode.left = root.left;
    copyNode.right = root.right;
    root = copyNode;
  }


  private void removeNodeWithoutBranch(Node<K, V> parentNode, Node<K, V> currentNode) {
    if (parentNode.left == currentNode) {
      parentNode.left = null;
    } else {
      parentNode.right = null;
    }
  }

  private void removeNodeWithLeftBranch(Node<K, V> parentNode, Node<K, V> currentNode) {
    if (parentNode.left == currentNode) {
      parentNode.left = currentNode.left;
    } else {
      parentNode.right = currentNode.left;
    }
  }

  private void removeNodeWithRightBranch(Node<K, V> parentNode, Node<K, V> currentNode) {
    if (parentNode.left == currentNode) {
      parentNode.left = currentNode.right;
    } else {
      parentNode.right = currentNode.right;
    }
  }

  private void removeNodeWithTwoBranches(Node<K, V> parentNode, Node<K, V> currentNode) {
    Node<K, V> copyNode = currentNode.right;
    while (copyNode.left != null) {
      copyNode = copyNode.left;
    }
    remove(copyNode.key);
    if (parentNode.left == currentNode) {
      parentNode.left = copyNode;
    } else {
      parentNode.right = copyNode;
    }
    copyNode.left = currentNode.left;
    copyNode.right = currentNode.right;
  }

  public void putAll(Map<? extends K, ? extends V> m) {
    for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
      put(entry.getKey(), entry.getValue());
    }
  }

  public void clear() {
    root = null;
    size = 0;
  }

  public Set<K> keySet() {
    Set<K> kSet = new HashSet<K>();
    recInOrderForSetOfKey(root, kSet);
    return kSet;
  }

  Node<K, V> recInOrderForSetOfKey(Node<K, V> currentNode, Set<K> keySet) {
    if (currentNode.left != null) {
      recInOrderForSetOfKey(currentNode.left, keySet);
    }
    keySet.add(currentNode.getKey());
    if (currentNode.right != null) {
      recInOrderForSetOfKey(currentNode.right, keySet);
    }
    return null;
  }

  public Node<K, V> getRoot() {
    return root;
  }

  void print(Node<K, V> currentNode) {
    if (currentNode.left != null) {
      print(currentNode.left);
    }
    System.out.println(currentNode.value);
    if (currentNode.right != null) {
      print(currentNode.right);
    }
  }

  public Collection<V> values() {
    Collection<V> value = new ArrayList<>();
    return value;
  }

  void recInOrderForValue(Node<K, V> currentNode, ArrayList<V> values) {
    if (currentNode.left != null) {
      recInOrderForValue(currentNode.left, values);
    }
    values.add(currentNode.value);
    if (currentNode.right != null) {
      recInOrderForValue(currentNode.right, values);
    }
  }


  public Set<Entry<K, V>> entrySet() {
    return null;
  }


  private static class Node<K extends Comparable, V> implements Map.Entry<K, V> {

    private Node<K, V> left;
    private Node<K, V> right;
    private K key;
    private V value;

    public Node(K key, V value) {
      this.key = key;
      this.value = value;
    }

    public K getKey() {
      return key;
    }

    public V getValue() {
      return value;
    }

    public V setValue(V value) {
      this.value = value;
      return value;
    }

    public int compareTo(K k) {
      return this.key.compareTo(k);
    }

    public int compareTo(Object k) {
      return this.key.compareTo(k);
    }
  }
}
