package bintree;

@FunctionalInterface
public interface Printable {

  void print();
}
